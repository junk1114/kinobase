@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $country))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('countries::edit', ['id' => $country->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                {{ $country->name }} ({{ $country->films->count() }})
            </h3>
            <hr>
        </div>
        <div class="col-md-12">
            @foreach($country->filmsPaginated as $film)
                @include('_film')
            @endforeach
        </div>
        <div class="col-md-12">
            {!! $country->filmsPaginated->render() !!}
        </div>
    </div>
@endsection
