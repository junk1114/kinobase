<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('countries')->truncate();
        $countries = [
            ['name' => 'Россия'],
            ['name' => 'США'],
            ['name' => 'СССР'],
            ['name' => 'Германия'],
        ];
        DB::table('countries')->insert($countries);
    }
}
