<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['name', 'description', 'year', 'rating', 'kinopoisk_id', 'original_name'];

    public function getPosterAttribute($value)
    {
        if (!($this->attributes['kinopoisk_id'])) {
            $poster = '//st.kp.yandex.net/images/movies/poster_none.png';
        } else {
            $poster = '//st.kp.yandex.net/images/film_big/' . $this->attributes['kinopoisk_id'] . '.jpg';
        }

        return $poster;
    }

    public function getKinopoiskLinkAttribute($value)
    {
        $link = '//kinopoisk.ru/film/' . $this->attributes['kinopoisk_id'] . '/';

        return $link;
    }

    public function getYearAttribute($value) {
        if ($value == 0) {
            return '';
        }
        else {
            return $value;
        }
    }

    /**
     * Убираем пробелы из имени
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }

    /**
     * Жанры фильма
     */
    public function genres()
    {
        return $this->belongsToMany('App\Genre');
    }

    /**
     * Режиссеры фильма
     */
    public function directors()
    {
        return $this->belongsToMany('App\Person', 'director_film');
    }

    /**
     * Актеры фильма
     */
    public function actors()
    {
        return $this->belongsToMany('App\Person', 'actor_film');
    }

    /**
     * Каталоги, в которые входит фильм
     */
    public function catalogs()
    {
        return $this->belongsToMany('App\Catalog');
    }

    /**
     * Страны, к которым принадлежит фильм
     */
    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

}
