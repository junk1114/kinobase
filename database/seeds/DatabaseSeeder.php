<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(FilmsTableSeeder::class);
        $this->call(GenresTableSeeder::class);
        $this->call(FilmGenreTableSeeder::class);
        $this->call(PeopleTableSeeder::class);
        $this->call(CatalogsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);

        Model::reguard();
    }
}
