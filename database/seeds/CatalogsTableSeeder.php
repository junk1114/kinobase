<?php

use Illuminate\Database\Seeder;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('catalogs')->truncate();
        $lists = [
            ['name' => 'Посмотреть'],
            ['name' => 'Скачать'],
            ['name' => 'Скачано'],
            ['name' => 'Любимые'],
            ['name' => 'Тяжелые'],
        ];
        DB::table('catalogs')->insert($lists);
    }
}
