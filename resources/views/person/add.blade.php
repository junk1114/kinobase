@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('success'))
                <div class="alert alert-success">
                    <p>Добавлено</p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route' => 'people::store']) !!}
                @include('person._form')
                <div class="form-group">
                    {!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
