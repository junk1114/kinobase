<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('genres')->truncate();
        $genres = [
            ['name' => 'Комедия'],
            ['name' => 'Ужасы'],
            ['name' => 'Драма'],
            ['name' => 'Вестерн'],
            ['name' => 'Фантастика'],
            ['name' => 'Приключения'],
            ['name' => 'Боевик'],
        ];
        DB::table('genres')->insert($genres);
    }
}
