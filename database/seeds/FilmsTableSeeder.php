<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $films = [
            ['name' => 'Жить', 'kinopoisk_id' => '521526', 'year' => '2010'],
            ['name' => 'Дурак', 'kinopoisk_id' => '808639', 'year' => '2014'],
            ['name' => 'Джанго освобожденный', 'kinopoisk_id' => '586397', 'year' => '2012'],
            ['name' => 'Бесславные ублюдки', 'kinopoisk_id' => '9691', 'year' => '2009'],
            ['name' => 'Аватар', 'kinopoisk_id' => '251733', 'year' => '2009'],
        ];
        DB::table('films')->insert($films);
    }
}
