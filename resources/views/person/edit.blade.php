@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $person))

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('success'))
                <div class="alert alert-success">
                    <p>Изменен</p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::model($person, ['route' => ['people::update', $person->id], 'method' => 'put']) !!}
                @include('person._form')
                <div class="form-group">
                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            {!! Form::open(['route' => ['people::remove', $person->id], 'method' => 'delete', 'onsubmit' => 'return confirm("Удалить?");']) !!}
                {!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
