<div class="panel panel-default">
    <div class="panel-body">
        <div class="media">
            <div class="media-left">
                <p><img class="media-object" src="{{ $film->poster }}" alt="" width="100"></p>
                @if($film->rating == 1)
                    <span class="btn btn-xs btn-success" title="Понравился"><i class="fa fa-thumbs-up"></i></span>
                @elseif ($film->rating == 2)
                    <span class="btn btn-xs btn-warning" title="Не просмотрен"><i class="fa fa-eye-slash"></i></span>
                @elseif ($film->rating == 0)
                    <span class="btn btn-xs btn-danger" title="Не понравился"><i class="fa fa-thumbs-down"></i></span>
                @endif
                <a href="{{ route('films::edit', ['id' => $film->id]) }}" class="btn btn-default btn-xs" title="Редактировать"><i class="fa fa-pencil"></i></a>
                @if($film->kinopoisk_id)
                    <a href="{{ $film->kinopoisk_link }}" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-film"></i></a>
                @endif
            </div>
            <div class="media-body">
                <div class="col-md-12">
                    <h4 class="media-heading">
                        <strong>
                            <a href="{{ route('films::show', ['id' => $film->id]) }}">
                                {{ $film->name }}
                            </a>
                        </strong>
                    </h4>
                    <p class="text-muted">{{ $film->year }}</p>
                    <p>
                        @foreach($film->countries as $country)
                            <span class="label label-primary"><a href="{{ route('countries::show', ['id' => $country->id]) }}"><i class="fa fa-globe"></i> {{ $country->name }}</a></span>
                        @endforeach
                    </p>
                    <p>
                        @foreach($film->genres as $genre)
                            <span class="label label-success"><a href="{{ route('genres::show', ['id' => $genre->id]) }}"><i class="fa fa-tag"></i> {{ $genre->name }}</a></span>
                        @endforeach
                    </p>
                    <p>
                        @foreach($film->catalogs as $catalog)
                            <span class="label label-warning"><a href="{{ route('catalogs::show', ['id' => $catalog->id]) }}"><i class="fa fa-folder-open-o"></i> {{ $catalog->name }}</a></span>
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
