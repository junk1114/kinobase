@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $film))

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="thumbnail">
                <img src="{{ $film->poster }}" alt="">
            </div>
            @if($film->rating == 1)
                <span class="btn btn-success" title="Понравился"><i class="fa fa-thumbs-up"></i></span>
            @elseif ($film->rating == 2)
                <span class="btn btn-warning" title="Не просмотрен"><i class="fa fa-eye-slash"></i></span>
            @elseif ($film->rating == 0)
                <span class="btn btn-danger" title="Не понравился"><i class="fa fa-thumbs-down"></i></span>
            @endif
        </div>
        <div class="col-md-6">
            <h4>
                <a href="{{ route('films::edit', ['id' => $film->id]) }}" class="btn btn-default btn-xs" title="Редактировать"><i class="fa fa-pencil"></i></a>
                {{ $film->name }}
            </h4>
            <p class="text-muted">{{ $film->original_name }}</p>
            <p>
                <a href="//www.google.ru/search?q={{ $film->name }} {{$film->year }} смотреть онлайн" target="_blank" class="btn btn-default"><i class="fa fa-search"></i> Просмотр</a>
                <a href="//www.google.ru/search?q={{ $film->name }} {{$film->year }} скачать торрент" target="_blank" class="btn btn-default"><i class="fa fa-search"></i> Торрент</a>
                @if($film->kinopoisk_id)
                    <a href="{{ $film->kinopoisk_link }}" class="btn btn-default" target="_blank"><i class="fa fa-film"></i> Кинопоиск</a>
                @endif
            </p>
            <p><strong>Год выхода:</strong> {{ $film->year }}</p>
            <p><strong>Жанры:</strong></p>
            <p>
                @foreach ($film->genres as $genre)
                    <span class="label label-primary"><a href="{{ route('genres::show', ['id' => $genre->id]) }}">{{ $genre->name }}</a></span>
                @endforeach
            </p>
            <p><strong>Каталоги:</strong></p>
            <p>
                @foreach ($film->catalogs as $catalog)
                    <span class="label label-primary"><a href="{{ route('catalogs::show', ['id' => $catalog->id]) }}">{{ $catalog->name }}</a></span>
                @endforeach
            </p>
            <p><strong>Страны:</strong></p>
            <p>
                @foreach ($film->countries as $country)
                    <span class="label label-primary"><a href="{{ route('countries::show', ['id' => $country->id]) }}">{{ $country->name }}</a></span>
                @endforeach
            </p>
            <p><strong>Режиссеры:</strong></p>
            <p>
                @foreach ($film->directors as $director)
                    <span class="label label-primary"><a href="{{ route('people::show', ['id' => $director->id]) }}">{{ $director->name }}</a></span>
                @endforeach
            </p>
            <p><strong>Актеры:</strong></p>
            <p>
                @foreach ($film->actors as $actor)
                    <span class="label label-primary"><a href="{{ route('people::show', ['id' => $actor->id]) }}">{{ $actor->name }}</a></span>
                @endforeach
            </p>
            <p><strong>Описание:</strong></p>
            <p>{!! nl2br(e($film->description)) !!}</p>
        </div>
    </div>
@endsection
