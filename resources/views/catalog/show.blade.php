@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $catalog))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('catalogs::edit', ['id' => $catalog->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                {{ $catalog->name }} ({{ $catalog->films->count() }})
            </h3>
            <hr>
        </div>
        <div class="col-md-12">
            @foreach($catalog->filmsPaginated as $film)
                @include('_film')
            @endforeach
        </div>
        <div class="col-md-12">
            {!! $catalog->filmsPaginated->render() !!}
        </div>
    </div>
@endsection
