<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['as' => 'films::'], function() {
    // autocomplete json
    Route::get('/films/ajax_search', [
        'as' => 'ajax_search',
        'uses' => 'FilmController@ajaxSearch'
    ]);
    // All films
    Route::get('/', [
        'as' => 'all',
        'uses' => 'FilmController@index'
    ]);
    // Add film
    Route::get('films/add', [
        'as' => 'create',
        'uses' => 'FilmController@create'
    ]);
    // Store film
    Route::post('films/add', [
        'as' => 'store',
        'uses' => 'FilmController@store'
    ]);
    // Search films
    Route::get('films/search', [
        'as' => 'search',
        'uses' => 'FilmController@search'
    ]);
    // Show film
    Route::get('films/{id}', [
        'as' => 'show',
        'uses' => 'FilmController@show'
    ]);
    // Edit film
    Route::get('films/{id}/edit', [
        'as' => 'edit',
        'uses' => 'FilmController@edit'
    ]);
    // Update film
    Route::put('films/{id}', [
        'as' => 'update',
        'uses' => 'FilmController@update'
    ]);
    // Remove film
    Route::delete('films/{id}', [
        'as' => 'remove',
        'uses' => 'FilmController@destroy'
    ]);
});

Route::group(['as' => 'catalogs::'], function() {
    // All
    Route::get('/catalogs', [
        'as' => 'all',
        'uses' => 'CatalogController@index'
    ]);
    // Add
    Route::get('catalogs/add', [
        'as' => 'create',
        'uses' => 'CatalogController@create'
    ]);
    // Store
    Route::post('catalogs/add', [
        'as' => 'store',
        'uses' => 'CatalogController@store'
    ]);
    // // Show
    Route::get('catalogs/{id}', [
        'as' => 'show',
        'uses' => 'CatalogController@show'
    ]);
    // Edit
    Route::get('catalogs/{id}/edit', [
        'as' => 'edit',
        'uses' => 'CatalogController@edit'
    ]);
    // // Update
    Route::put('catalogs/{id}', [
        'as' => 'update',
        'uses' => 'CatalogController@update'
    ]);
    // // Remove
    Route::delete('catalogs/{id}', [
        'as' => 'remove',
        'uses' => 'CatalogController@destroy'
    ]);
});

Route::group(['as' => 'genres::'], function() {
    // All
    Route::get('/genres', [
        'as' => 'all',
        'uses' => 'GenreController@index'
    ]);
    // Add
    Route::get('genres/add', [
        'as' => 'create',
        'uses' => 'GenreController@create'
    ]);
    // Store
    Route::post('genres/add', [
        'as' => 'store',
        'uses' => 'GenreController@store'
    ]);
    // // Show
    Route::get('genres/{id}', [
        'as' => 'show',
        'uses' => 'GenreController@show'
    ]);
    // Edit
    Route::get('genres/{id}/edit', [
        'as' => 'edit',
        'uses' => 'GenreController@edit'
    ]);
    // // Update
    Route::put('genres/{id}', [
        'as' => 'update',
        'uses' => 'GenreController@update'
    ]);
    // // Remove
    Route::delete('genres/{id}', [
        'as' => 'remove',
        'uses' => 'GenreController@destroy'
    ]);
});

Route::group(['as' => 'countries::'], function() {
    // All
    Route::get('/countries', [
        'as' => 'all',
        'uses' => 'CountryController@index'
    ]);
    // Add
    Route::get('countries/add', [
        'as' => 'create',
        'uses' => 'CountryController@create'
    ]);
    // Store
    Route::post('countries/add', [
        'as' => 'store',
        'uses' => 'CountryController@store'
    ]);
    // // Show
    Route::get('countries/{id}', [
        'as' => 'show',
        'uses' => 'CountryController@show'
    ]);
    // Edit
    Route::get('countries/{id}/edit', [
        'as' => 'edit',
        'uses' => 'CountryController@edit'
    ]);
    // // Update
    Route::put('countries/{id}', [
        'as' => 'update',
        'uses' => 'CountryController@update'
    ]);
    // // Remove
    Route::delete('countries/{id}', [
        'as' => 'remove',
        'uses' => 'CountryController@destroy'
    ]);
});

Route::group(['as' => 'people::'], function() {
    // autocomplete json
    Route::get('/people/ajax_search', [
        'as' => 'ajax_search',
        'uses' => 'PersonController@ajaxSearch'
    ]);
    // All
    Route::get('/people', [
        'as' => 'all',
        'uses' => 'PersonController@index'
    ]);
    // Add
    Route::get('people/add', [
        'as' => 'create',
        'uses' => 'PersonController@create'
    ]);
    // Store
    Route::post('people/add', [
        'as' => 'store',
        'uses' => 'PersonController@store'
    ]);
    // // Show
    Route::get('people/{id}', [
        'as' => 'show',
        'uses' => 'PersonController@show'
    ]);
    // Edit
    Route::get('people/{id}/edit', [
        'as' => 'edit',
        'uses' => 'PersonController@edit'
    ]);
    // // Update
    Route::put('people/{id}', [
        'as' => 'update',
        'uses' => 'PersonController@update'
    ]);
    // // Remove
    Route::delete('people/{id}', [
        'as' => 'remove',
        'uses' => 'PersonController@destroy'
    ]);
});
