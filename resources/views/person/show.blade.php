@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $person))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('people::edit', ['id' => $person->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                {{ $person->name }}
            </h3>
            <hr>
            <h3>Актер ({{ $person->actors->count() }})</h3>
            <ul>
                @foreach($person->actors as $film)
                    <li><a href="{{ route('films::show', ['id' => $film->id]) }}">{{ $film->name }} ({{ $film->year }})</a></li>
                @endforeach
            </ul>
            <h3>Режиссер ({{ $person->directors->count() }})</h3>
            <ul>
                @foreach($person->directors as $film)
                    <li><a href="{{ route('films::show', ['id' => $film->id]) }}">{{ $film->name }} ({{ $film->year }})</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
