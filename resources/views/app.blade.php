<!DOCTYPE html>
<html>
    <head>
        <meta>
        <title>Kinobase</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="/assets/css/select2.min.css">
        <link rel="stylesheet" href="/assets/css/easy-autocomplete.min.css">
        <link rel="stylesheet" href="/assets/css/easy-autocomplete.themes.min.css">
        <link rel="stylesheet" href="/assets/css/main.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container">
                <div class="row header">
                    <div class="col-md-5">
                        <h1><a href="/">Kinobase</a></h1>
                    </div>
                    <div class="col-md-7">
                        <a href="{{ route('films::all') }}" class="btn btn-xs btn-default">Фильмы</a>
                        <a href="{{ route('catalogs::all') }}" class="btn btn-xs btn-default">Каталоги</a>
                        <a href="{{ route('genres::all') }}" class="btn btn-xs btn-default">Жанры</a>
                        <a href="{{ route('countries::all') }}" class="btn btn-xs btn-default">Страны</a>
                        <a href="{{ route('people::all') }}" class="btn btn-xs btn-default">Люди</a>
                    </div>
                </div>
                <div class="row breadcrumbs">
                    <div class="col-md-12">
                        @yield('breadcrumbs')
                    </div>
                </div>
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
        <script src="//yastatic.net/jquery/2.1.4/jquery.min.js"></script>
        <script src="//yastatic.net/underscore/1.6.0/underscore-min.js"></script>
        <script src="//yastatic.net/backbone/1.1.2/backbone-min.js"></script>
        <script src="/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="/assets/js/jquery.are-you-sure.js"></script>
        <script src="/assets/js/select2.min.js"></script>
        <script src="/assets/js/select2.ru.js"></script>
        <script src="/assets/js/jquery.easy-autocomplete.min.js"></script>
        <script src="/assets/js/main.js"></script>
    </body>
</html>
