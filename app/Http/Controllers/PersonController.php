<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Person;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PersonRequest;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['people'] = Person::orderBy('name')->paginate(100);

        return view('person.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        return view('person.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonRequest $request)
    {
        $input = $request->all();
        // dd($input);
        $film = Person::create($input);

        return back()->with('success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['person'] = Person::with(
        [
            'directors' => function($q) {
                $q->orderBy('name');
            }
        ],
        [
            'actors' => function($q) {
                $q->orderBy('name');
            }
        ]
        )->findOrFail($id);

        return view('person.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['person'] = Person::findOrFail($id);

        return view('person.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonRequest $request, $id)
    {
        $person = Person::findOrFail($id);
        $input = $request->all();
        $person->update($input);

        return back()->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::findOrFail($id);
        $person->destroy($id);

        return redirect()->route('people::all');
    }

    /**
     * Поиск, вывод в Json
     */
    public function ajaxSearch(Request $request)
    {
        $person = Person::where('name', 'like', '%' . $request->term . '%')->get();

        return $person;
    }
}
