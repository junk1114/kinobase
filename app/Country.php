<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    /**
     * Фильмы, принадлежащие стране
     */
    public function films()
    {
        return $this->belongsToMany('App\Film');
    }
    
    /**
     * Пагинация фильмов
     */
    public function getFilmsPaginatedAttribute()
    {
        return $this->films()->orderBy('name')->paginate(10);
    }

    /**
     * Убираем пробелы из имени
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }
}
