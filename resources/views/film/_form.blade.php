<div class="form-group">
    <span class="red-star">*</span>
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'autocomplete' => 'off', 'required' => '']) !!}
</div>
<div class="form-group">
    {!! Form::label('original_name', 'Оригинальное название (если есть):') !!}
    {!! Form::text('original_name', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
</div>
<div class="form-group">
    {!! Form::label('year', 'Год выхода:') !!}
    {!! Form::number('year', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
</div>
<div class="form-group">
    {!! Form::label('rating', 'Рейтинг:') !!}
    {!! Form::select('rating', ['2' => 'Не смотрел', '0' => 'Не понравился', '1' => 'Понравился'], null, ['id' => 'rating', 'class' => 'form-control select2']) !!}
</div>
<div class="form-group">
    {!! Form::label('genres', 'Жанры:') !!}
    <select name="genres[]" id="genres" class="form-control select2" data-placeholder="Жанры" multiple>
        @foreach($genres as $genre)
            @if(old('genres'))
                <option value="{{ $genre->id }}" {{ in_array($genre->id, old('genres')) ? 'selected' : '' }}>{{ $genre->name }}</option>
            @elseif(isset($film))
                <option value="{{ $genre->id }}" {{ $film->genres->contains($genre->id) ? 'selected' : '' }}>{{ $genre->name }}</option>
            @else
                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    {!! Form::label('catalogs', 'Каталоги:') !!}
    <select name="catalogs[]" id="catalogs" class="form-control select2" data-placeholder="Каталоги" multiple>
        @foreach($catalogs as $catalog)
            @if(old('catalogs'))
                <option value="{{ $catalog->id }}" {{ in_array($catalog->id, old('catalogs')) ? 'selected' : '' }}>{{ $catalog->name }}</option>
            @elseif(isset($film))
                <option value="{{ $catalog->id }}" {{ $film->catalogs->contains($catalog->id) ? 'selected' : '' }}>{{ $catalog->name }}</option>
            @else
                <option value="{{ $catalog->id }}">{{ $catalog->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    {!! Form::label('countries', 'Страны:') !!}
    <select name="countries[]" id="countries" class="form-control select2" data-placeholder="Страны" multiple>
        @foreach($countries as $country)
            @if(old('countries'))
                <option value="{{ $country->id }}" {{ in_array($country->id, old('countries')) ? 'selected' : '' }}>{{ $country->name }}</option>
            @elseif(isset($film))
                <option value="{{ $country->id }}" {{ $film->countries->contains($country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
            @else
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">
    {!! Form::label('directors', 'Режиссеры:') !!}
    <select name="directors[]" id="directors" class="form-control select2-remote-people" data-placeholder="Режиссеры" multiple>
        @if(isset($film))
            @foreach($film->directors as $person)
                <option value="{{ $person->id }}" selected>{{ $person->name }}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    {!! Form::label('actors', 'Актеры:') !!}
    <select name="actors[]" id="actors" class="form-control select2-remote-people" data-placeholder="Актеры" multiple>
        @if(isset($film))
            @foreach($film->actors as $person)
                <option value="{{ $person->id }}" selected>{{ $person->name }}</option>
            @endforeach
        @endif
    </select>
    {{-- <select name="actors[]" id="actors" class="form-control select2 chosen-ajax" data-placeholder="Актеры" multiple>
        @foreach($people as $person)
            @if(old('actors'))
                <option value="{{ $person->id }}" {{ in_array($person->id, old('actors')) ? 'selected' : '' }}>{{ $person->name }}</option>
            @elseif(isset($film))
                <option value="{{ $person->id }}" {{ $film->actors->contains($person->id) ? 'selected' : '' }}>{{ $person->name }}</option>
            @else
                <option value="{{ $person->id }}">{{ $person->name }}</option>
            @endif
        @endforeach
    </select> --}}
</div>
<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('kinopoisk_id', 'Кинопоиск ID:') !!}
    {!! Form::number('kinopoisk_id', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
</div>
