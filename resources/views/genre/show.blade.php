@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $genre))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('genres::edit', ['id' => $genre->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                {{ $genre->name }} ({{ $genre->films->count() }})
            </h3>
            <hr>
        </div>
        <div class="col-md-12">
            @foreach($genre->filmsPaginated as $film)
                @include('_film')
            @endforeach
        </div>
        <div class="col-md-12">
            {!! $genre->filmsPaginated->render() !!}
        </div>
    </div>
@endsection
