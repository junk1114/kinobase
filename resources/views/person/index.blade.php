@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('people::create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
            <hr>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Актер</th>
                        <th>Режиссер</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($people as $person)
                        <tr>
                            <td>
                                <a href="{{ route('people::edit', ['id' => $person->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('people::show', ['id' => $person->id]) }}">{{ $person->name }}</a>
                            </td>
                            <td>{{ $person->actors()->count() }}</td>
                            <td>{{ $person->directors()->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            {!! $people->render() !!}
        </div>
    </div>
@endsection
