@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName(), $film))

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="thumbnail">
                <img src="{{ $film->poster }}" alt="">
            </div>
        </div>
        <div class="col-md-6">
            @if (session('success'))
                <div class="alert alert-success">
                    <p>Фильм изменен</p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p>
                <a href="//www.google.ru/search?q={{ $film->name }} {{$film->year }} смотреть онлайн" target="_blank" class="btn btn-default"><i class="fa fa-search"></i> Просмотр</a>
                <a href="//www.google.ru/search?q={{ $film->name }} {{$film->year }} скачать торрент" target="_blank" class="btn btn-default"><i class="fa fa-search"></i> Торрент</a>
                @if($film->kinopoisk_id)
                    <a href="{{ $film->kinopoisk_link }}" class="btn btn-default" target="_blank"><i class="fa fa-film"></i> Кинопоиск</a>
                @endif
            </p>
            {!! Form::model($film, ['route' => ['films::update', $film->id], 'method' => 'put']) !!}
                @include('film._form')
                <div class="form-group">
                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            {!! Form::open(['route' => ['films::remove', $film->id], 'method' => 'delete', 'onsubmit' => 'return confirm("Удалить?");']) !!}
                {!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
