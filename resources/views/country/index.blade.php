@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('countries::create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
            <hr>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Фильмы</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries as $country)
                        <tr>
                            <td>
                                <a href="{{ route('countries::edit', ['id' => $country->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('countries::show', ['id' => $country->id]) }}">{{ $country->name }}</a>
                            </td>
                            <td>{{ $country->films()->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            {!! $countries->render() !!}
        </div>
    </div>
@endsection
