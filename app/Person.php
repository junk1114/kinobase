<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = ['name', 'original_name'];

    public $timestamps = false;

    public function setOriginalNameAttribute($value)
    {
        if ($value == '') {
            $this->attributes['original_name'] = NULL;
        }
        else {
            $this->attributes['original_name'] = $value;
        }
    }

    /**
     * Убираем пробелы из имени
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }
        
    /**
     * В каких фильмах человек - режиссер
     */
    public function directors()
    {
        return $this->belongsToMany('App\Film', 'director_film');
    }

    /**
     * В каких фильмах человек - актер
     */
    public function actors()
    {
        return $this->belongsToMany('App\Film', 'actor_film');
    }

}
