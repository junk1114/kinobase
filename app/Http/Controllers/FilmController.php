<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Film;
use App\Genre;
use App\Person;
use App\Catalog;
use App\Country;
use App\Http\Requests\FilmRequest;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // dd($request->all());
        // $request->session()->flush();
        $data['genres'] = Genre::orderBy('name')->get();
        $data['people'] = Person::orderBy('name')->get();
        $data['catalogs'] = Catalog::orderBy('name')->get();
        $data['countries'] = Country::orderBy('name')->get();
        $data['films'] = Film::with('genres')->orderBy('id', 'desc')->paginate(10);

        return view('film.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data['genres'] = Genre::orderBy('name')->get();
        // $data['people'] = Person::orderBy('name')->get();
        $data['catalogs'] = Catalog::orderBy('name')->get();
        $data['countries'] = Country::orderBy('name')->get();

        return view('film.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FilmRequest $request)
    {
        $input = $request->all();
        // dd($input);
        $film = Film::create($input);
        // For sync
        $syncing = ['genres', 'directors', 'actors', 'catalogs', 'countries'];
        foreach ($syncing as $item) {
            if (!isset($input[$item])) {
                $for_sync = [];
            } else {
                $for_sync = $input[$item];
            }
            $film->{$item}()->sync($for_sync);
        }

        return back()->with('success', true);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $data['film'] = Film::findOrFail($id);

        return view('film.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // dd(old());
        // $request->session()->flush();
        $data['film'] = Film::findOrFail($id);
        $data['genres'] = Genre::orderBy('name')->get();
        // $data['people'] = Person::orderBy('name')->get();
        $data['catalogs'] = Catalog::orderBy('name')->get();
        $data['countries'] = Country::orderBy('name')->get();

        return view('film.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(FilmRequest $request, $id)
    {
        $film = Film::findOrFail($id);
        $input = $request->all();
        // dd($input);
        $film->update($input);
        // For sync
        $syncing = ['genres', 'directors', 'actors', 'catalogs', 'countries'];
        foreach ($syncing as $item) {
            if (!isset($input[$item])) {
                $for_sync = [];
            } else {
                $for_sync = $input[$item];
            }
            $film->{$item}()->sync($for_sync);
        }

        return back()->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $film = Film::findOrFail($id);
        $film->destroy($id);

        return redirect('/');
    }

    /**
     * Фильтрация фильмов
     */
    public function search(Request $request)
    {
        // dd($request->all());
        $data['genres'] = Genre::orderBy('name')->get();
        $data['people'] = Person::orderBy('name')->get();
        $data['catalogs'] = Catalog::orderBy('name')->get();
        $data['countries'] = Country::orderBy('name')->get();
        $films = Film::with('genres')->where('name', 'like', '%' . $request->name . '%');
        if ($request->year1 && $request->year2) {
            $films = $films->whereBetween('year', [$request->year1, $request->year2]);
        }
        if (in_array($request->rating, [0, 1, 2])) {
            $films->where('rating', $request->rating);
        }
        if ($request->catalogs) {
            foreach ($request->catalogs as $catalog) {
                $films = $films->whereHas('catalogs', function ($query) use($catalog) {
                    $query->where('id', $catalog);
                });
            }
        }
        if ($request->genres) {
            foreach ($request->genres as $genre) {
                $films = $films->whereHas('genres', function ($query) use($genre) {
                    $query->where('id', $genre);
                });
            }
        }
        if ($request->countries) {
            foreach ($request->countries as $country) {
                $films = $films->whereHas('countries', function ($query) use($country) {
                    $query->where('id', $country);
                });
            }
        }
        if ($request->actors) {
            foreach ($request->actors as $person) {
                $films = $films->whereHas('actors', function ($query) use($person) {
                    $query->where('id', $person);
                });
            }
        }
        if ($request->directors) {
            foreach ($request->directors as $person) {
                $films = $films->whereHas('directors', function ($query) use($person) {
                    $query->where('id', $person);
                });
            }
        }
        $data['films'] = $films->orderBy('id', 'desc')->paginate(10);
        $data['request'] = $request->all();

        return view('film.index', $data);
    }

    /**
     * Поиск, вывод в Json
     */
    public function ajaxSearch(Request $request)
    {
        $film = Film::where('name', 'like', '%' . $request->term . '%')->get();

        return $film;
    }
}
