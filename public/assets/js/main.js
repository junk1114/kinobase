jQuery(document).ready(function($) {
    $(".select2").select2({
        language: "ru"
    });

    $(".select2-remote-people").select2({
        language: "ru",
        minimumInputLength: 3,
        ajax: {
            url: "/people/ajax_search",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
                return {
                    term: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            escapeMarkup: function (markup) { return markup; }
        }
    });

    $('.mfp-link').magnificPopup({
        type:'inline',
        midClick: true,
    });

    $('form').areYouSure({
        'message': 'У вас есть несохраненные изменения!'
    });

    $('.red-star').attr({
        title: 'Обязательное поле',
    });

    $('.fast-search-film').easyAutocomplete({
    	url: function(term) {
    		return '/films/ajax_search?term=' + term;
    	},
    	getValue: 'name',
        template: {
    		type: 'custom',
    		method: function(value, item) {
    			return '<a href="/films/ ' + item.id + '">' + item.name + ' (' + item.year + ')</a>';
    		}
    	}
    });

    $('.fast-search-person').easyAutocomplete({
    	url: function(term) {
    		return '/people/ajax_search?term=' + term;
    	},
    	getValue: 'name',
    });

});
