<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT') {
            $rules = [
                'name' => 'required|unique:countries,name,'.$this->id.',id|max:250',
            ];
        } elseif ($this->method() == 'POST') {
            $rules = [
                'name' => 'required|unique:countries,name|max:250',
            ];
        }

        return $rules;
    }
}
