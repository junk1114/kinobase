@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('films::create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
            <a href="#search-popup" class="btn btn-default mfp-link"><i class="fa fa-search"></i> Найти</a>
        </div>
        <div class="col-md-6">
            <input type="search" class="form-control fast-search-film" placeholder="Быстрый поиск фильма">
        </div>
        <div class="col-md-12">
            <div id="search-popup" class="white-popup mfp-hide">
                <h4>Найти фильм</h4>
                {!! Form::open(['route' => 'films::search', 'method' => 'get']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Название:') !!}
                        {!! Form::text('name', @$request['name'], ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('rating', 'Рейтинг:') !!}
                        {!! Form::select('rating', ['3' => '', '2' => 'Не смотрел', '0' => 'Не понравился', '1' => 'Понравился'], @$request['rating'], ['id' => 'rating', 'class' => 'form-control select2-single']) !!}
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('year1', 'Год выхода (с):') !!}
                                {!! Form::number('year1', @$request['year1'], ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('year2', 'Год выхода (по):') !!}
                                {!! Form::number('year2', @$request['year2'], ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('catalogs', 'Каталоги:') !!}
                        <select name="catalogs[]" id="catalogs" class="form-control select2" data-placeholder="Каталоги" multiple>
                            @foreach($catalogs as $catalog)
                                <option value="{{ $catalog->id }}" {{ @in_array($catalog->id, $request['catalogs']) ? 'selected' : '' }}>{{ $catalog->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {!! Form::label('genres', 'Жанры:') !!}
                        <select name="genres[]" id="genres" class="form-control select2" data-placeholder="Жанры" multiple>
                            @foreach($genres as $genre)
                                <option value="{{ $genre->id }}" {{ @in_array($genre->id, $request['genres']) ? 'selected' : '' }}>{{ $genre->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {!! Form::label('countries', 'Страны:') !!}
                        <select name="countries[]" id="countries" class="form-control select2" data-placeholder="Страны" multiple>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ @in_array($country->id, $request['countries']) ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {!! Form::label('actors', 'Актеры:') !!}
                        <select name="actors[]" id="actors" class="form-control select2-remote-people" data-placeholder="Актеры" multiple></select>
                    </div>
                    <div class="form-group">
                        {!! Form::label('directors', 'Режиссеры:') !!}
                        <select name="directors[]" id="directors" class="form-control select2-remote-people" data-placeholder="Режиссеры" multiple></select>
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Поиск', ['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <hr>
        </div>
        <div class="col-md-12">
            @foreach($films as $film)
                @include('_film')
            @endforeach
        </div>
        <div class="col-md-12">
            @if(isset($request))
                {!! $films->appends($request)->render() !!}
            @else
                {!! $films->render() !!}
            @endif
        </div>
    </div>
@endsection
