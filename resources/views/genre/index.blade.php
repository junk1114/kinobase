@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('genres::create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
            <hr>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Фильмы</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($genres as $genre)
                        <tr>
                            <td>
                                <a href="{{ route('genres::edit', ['id' => $genre->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('genres::show', ['id' => $genre->id]) }}">{{ $genre->name }}</a>
                            </td>
                            <td>{{ $genre->films()->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            {!! $genres->render() !!}
        </div>
    </div>
@endsection
