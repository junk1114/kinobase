<?php
    use Rocketeer\Facades\Rocketeer;
    
    Rocketeer::task('migrate', 'php artisan migrate --seed');
