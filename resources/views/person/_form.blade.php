<div class="form-group">
    {!! Form::label('name', 'Имя:') !!}
    {!! Form::text('name', null, ['class' => 'form-control fast-search-person', 'autocomplete' => 'off', 'autofocus' => '']) !!}
</div>
<div class="form-group">
    {!! Form::label('original_name', 'Имя на родном языке:') !!}
    {!! Form::text('original_name', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
</div>
