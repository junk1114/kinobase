<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $people = [
            ['name' => 'Юрий Быков'],
            ['name' => 'Джеймс Кэмерон'],
            ['name' => 'Квентин Тарантино'],
            ['name' => 'Элай Рот'],
        ];
        DB::table('people')->insert($people);
    }
}
