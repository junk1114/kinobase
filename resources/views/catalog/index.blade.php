@extends('app')

@section('breadcrumbs', Breadcrumbs::render(Route::getCurrentRoute()->getName()))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('catalogs::create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Добавить</a>
            <hr>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Фильмы</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($catalogs as $catalog)
                        <tr>
                            <td>
                                <a href="{{ route('catalogs::edit', ['id' => $catalog->id]) }}" class="btn btn-xs btn-default" title="Редактировать"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('catalogs::show', ['id' => $catalog->id]) }}">{{ $catalog->name }}</a>
                            </td>
                            <td>{{ $catalog->films()->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            {!! $catalogs->render() !!}
        </div>
    </div>
@endsection
