<?php

    /**
     * Фильмы
     */
    function getFilmNameWithYear($obj) {
        return $obj->year ? $obj->name . ' (' . $obj->year . ')' : $obj->name;
    }
    Breadcrumbs::register('films::all', function($breadcrumbs)
    {
        $breadcrumbs->push('Фильмы', route('films::all'));
    });
    Breadcrumbs::register('films::search', function($breadcrumbs)
    {
        $breadcrumbs->parent('films::all');
        $breadcrumbs->push('Результаты поиска', route('films::search'));
    });
    Breadcrumbs::register('films::show', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('films::all');
        $breadcrumbs->push(getFilmNameWithYear($obj), route('films::show', $obj->id));
    });
    Breadcrumbs::register('films::edit', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('films::all');
        $breadcrumbs->push(getFilmNameWithYear($obj), route('films::show', $obj->id));
        $breadcrumbs->push('Редактировать', route('films::edit', $obj->id));
    });
    Breadcrumbs::register('films::create', function($breadcrumbs)
    {
        $breadcrumbs->parent('films::all');
        $breadcrumbs->push('Добавить', route('films::create'));
    });

    /**
    * Каталоги
    */
    Breadcrumbs::register('catalogs::all', function($breadcrumbs)
    {
        $breadcrumbs->push('Каталоги', route('catalogs::all'));
    });
    Breadcrumbs::register('catalogs::show', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('catalogs::all');
        $breadcrumbs->push($obj->name, route('catalogs::show', $obj->id));
    });
    Breadcrumbs::register('catalogs::edit', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('catalogs::all');
        $breadcrumbs->push($obj->name, route('catalogs::show', $obj->id));
        $breadcrumbs->push('Редактировать', route('catalogs::edit', $obj->id));
    });
    Breadcrumbs::register('catalogs::create', function($breadcrumbs)
    {
        $breadcrumbs->parent('catalogs::all');
        $breadcrumbs->push('Добавить', route('catalogs::create'));
    });

    /**
    * Жанры
    */
    Breadcrumbs::register('genres::all', function($breadcrumbs)
    {
        $breadcrumbs->push('Жанры', route('genres::all'));
    });
    Breadcrumbs::register('genres::show', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('genres::all');
        $breadcrumbs->push($obj->name, route('genres::show', $obj->id));
    });
    Breadcrumbs::register('genres::edit', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('genres::all');
        $breadcrumbs->push($obj->name, route('genres::show', $obj->id));
        $breadcrumbs->push('Редактировать', route('genres::edit', $obj->id));
    });
    Breadcrumbs::register('genres::create', function($breadcrumbs)
    {
        $breadcrumbs->parent('genres::all');
        $breadcrumbs->push('Добавить', route('genres::create'));
    });

    /**
    * Страны
    */
    Breadcrumbs::register('countries::all', function($breadcrumbs)
    {
        $breadcrumbs->push('Страны', route('countries::all'));
    });
    Breadcrumbs::register('countries::show', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('countries::all');
        $breadcrumbs->push($obj->name, route('countries::show', $obj->id));
    });
    Breadcrumbs::register('countries::edit', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('countries::all');
        $breadcrumbs->push($obj->name, route('countries::show', $obj->id));
        $breadcrumbs->push('Редактировать', route('countries::edit', $obj->id));
    });
    Breadcrumbs::register('countries::create', function($breadcrumbs)
    {
        $breadcrumbs->parent('countries::all');
        $breadcrumbs->push('Добавить', route('countries::create'));
    });

    /**
    * Люди
    */
    Breadcrumbs::register('people::all', function($breadcrumbs)
    {
        $breadcrumbs->push('Люди', route('people::all'));
    });
    Breadcrumbs::register('people::show', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('people::all');
        $breadcrumbs->push($obj->name, route('people::show', $obj->id));
    });
    Breadcrumbs::register('people::edit', function($breadcrumbs, $obj)
    {
        $breadcrumbs->parent('people::all');
        $breadcrumbs->push($obj->name, route('people::show', $obj->id));
        $breadcrumbs->push('Редактировать', route('people::edit', $obj->id));
    });
    Breadcrumbs::register('people::create', function($breadcrumbs)
    {
        $breadcrumbs->parent('people::all');
        $breadcrumbs->push('Добавить', route('people::create'));
    });
