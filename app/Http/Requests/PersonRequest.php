<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PersonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Если сохранение
        if ($this->method() == 'PUT') {
            $rules = [
                'name' => 'required|unique:people,name,'.$this->id.',id|max:250',
                'original_name' => 'unique:people,original_name,'.$this->id.',id|max:250',
            ];
        } elseif ($this->method() == 'POST') {
            $rules = [
                'name' => 'required|unique:people,name|max:250',
                'original_name' => 'unique:people,original_name|max:250',
            ];
        }

        return $rules;
    }
}
